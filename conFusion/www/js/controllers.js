angular.module('conFusion.controllers',[])

//$ionicModal is a service injected for using modal form in the application
.controller('AppCtrl', function ($scope, $ionicModal, $timeout, $localStorage, $ionicPlatform, $cordovaCamera, $cordovaImagePicker) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal [ binding for the formData ]
  //$scope.loginData = {};
  $scope.loginData = $localStorage.getObject('userinfo','{}');
  $scope.reservation = {};
  $scope.registration = {};
    
  $scope.favoritesData = $localStorage.getObject('favorites','{}');
    
   //modal for reservation modal
  $ionicModal.fromTemplateUrl('templates/reserve.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal= modal;
  });
    
  // Triggered in the login modal to close it
  $scope.closeReserve = function() {
    $scope.modal.hide();
  };

  // Open the login modal called by ng-click="reserve()"
  $scope.reserve = function() {
    $scope.modal.show();
  };
    
   // Perform the login action when the user submits the login form
  $scope.doReserve = function() {
    console.log('Doing reservation', $scope.reservation);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
      
    //Here we handle the process of the form to the server for satisfy the
    //the request to the server.
      
    $timeout(function() {
      $scope.closeReserve();
    }, 1000);
  };
    
  // Create the comment modal that we will use later [ creation of the modal ]
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal called by ng-click="login()"
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
      
    console.log('Doing login', $scope.loginData);
    //Usinfg local storage for the data of the login form
    $localStorage.storeObject('userinfo',$scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
      
    //Here we handle the process of the form to the server for satisfy the
    //the request to the server.
      
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  }; 
    
  // Create the registration modal that we will use later
    $ionicModal.fromTemplateUrl('templates/register.html', {
        scope: $scope
    }).then(function (modal) {
        $scope.registerform = modal;
    });

    // Triggered in the registration modal to close it
    $scope.closeRegister = function () {
        $scope.registerform.hide();
    };

    // Open the registration modal
    $scope.register = function () {
        $scope.registerform.show();
    };

    // Perform the registration action when the user submits the registration form
    $scope.doRegister = function () {
        console.log('Doing reservation', $scope.reservation);

        // Simulate a registration delay. Remove this and replace with your registration
        // code if using a registration system
        $timeout(function () {
            $scope.closeRegister();
        }, 1000);
    };
    
    //Ready function for registration
    
     $ionicPlatform.ready(function() {
        var options = {
            quality: 50,
            //destinationType: Camera.DestinationType.DATA_URL,
            //sourceType: Camera.PictureSourceType.CAMERA,
            allowEdit: true,
            //encodingType: Camera.EncodingType.JPEG,
            targetWidth: 100,
            targetHeight: 100,
            //popoverOptions: CameraPopoverOptions,
            saveToPhotoAlbum: false
        };
        
        //Function for taking picture with mobile camera
        $scope.takePicture = function() {
            $cordovaCamera.getPicture(options).then(function(imageData) {
                $scope.registration.imgSrc = "data:image/jpeg;base64," + imageData;
            }, function(err) {
                console.log(err);
            });

            $scope.registerform.show();

        };
         
        //Code for manage the gallery
        var options = {
         maximumImagesCount: 10,
         width: 100,
         height: 100,
         quality: 50
         };
        
         //Function for display the gallery
        $scope.gallery = function() {
         $cordovaImagePicker.getPictures(options)
         .then(function(results) {
            for (var i = 0; i < results.length; i++) {
                console.log('Image URI: ' + results[i]);
                $scope.registration.imgSrc = results[i];
            }
       
        }, function(error) {
            //a way for display error to user
        });
            $scope.registerform.show();

        }; 
    });
    
}).controller('MenuController', ['$scope', 'dishes', 'favoriteFactory', 'baseURL', '$ionicListDelegate', '$ionicPlatform', '$cordovaLocalNotification', '$cordovaToast', '$cordovaVibration', function ($scope, dishes, favoriteFactory, baseURL, $ionicListDelegate, $ionicPlatform, $cordovaLocalNotification, $cordovaToast) {
            
    $scope.baseURL = baseURL;
    $scope.tab = 1;
    $scope.filtText = '';
    $scope.showDetails = false;

    //Using resources
    $scope.showMenu = false;
    $scope.message = "Loading ...";
      
    //Variable injected from app.js with execute query()
    $scope.dishes = dishes;
    
    $scope.select = function(setTab) {
        $scope.tab = setTab;
        
        if (setTab === 2) {
            $scope.filtText = "appetizer";
        }
        else if (setTab === 3) {
            $scope.filtText = "mains";
        }
        else if (setTab === 4) {
            $scope.filtText = "dessert";
        }
        else {
            $scope.filtText = "";
        }
    };
    
    $scope.isSelected = function (checkTab) {
        return ($scope.tab === checkTab);
    };
    
    $scope.toggleDetails = function() {
        $scope.showDetails = !$scope.showDetails;
    };
    
    $scope.addFavorite = function (index) {
        favoriteFactory.addToFavorites(index);
        $ionicListDelegate.closeOptionButtons();
        
        $ionicPlatform.ready(function () {
                $cordovaLocalNotification.schedule({
                    id: 1,
                    title: "Added Favorite",
                    text: $scope.dishes[index].name
                }).then(function () {
                    console.log('Added Favorite '+$scope.dishes[index].name);
                },
                function () {
                    console.log('Failed to add Notification ');
                });

                $cordovaToast
                  .show('Added Favorite '+$scope.dishes[index].name, 'long', 'center')
                  .then(function (success) {
                      // success
                  }, function (error) {
                      // error
                  });
        });
        
        
    }
}]).controller('ContactController', ['$scope', function($scope) {
    
    $scope.feedback = {mychannel:"", firstName:"", lastName:"", agree:false, email:"" };
    
    var channels = [{value:"tel", label:"Tel."}, {value:"Email",label:"Email"}];
    
    $scope.channels = channels;
    $scope.invalidChannelSelection = false;
    
}]).controller('FeedbackController', ['$scope', 'feedbackFactory', function($scope,feedbackFactory) {
            
            $scope.sendFeedback = function() {
                
                console.log($scope.feedback);
				
                if ($scope.feedback.agree && ($scope.feedback.mychannel == "")) {
                    $scope.invalidChannelSelection = true;
                    console.log('incorrect');
                }
                else {
                    $scope.invalidChannelSelection = false;
					feedbackFactory.getFeedback().save($scope.feedback); 
                    $scope.feedback = {mychannel:"", firstName:"", lastName:"", agree:false, email:"" };
                    $scope.feedback.mychannel="";
                    $scope.feedbackForm.$setPristine();
                    console.log($scope.feedback);
                }
            };
        }]).controller('DishDetailController', ['$scope', '$stateParams', '$ionicPopover', '$ionicModal', '$timeout', '$ionicLoading', 'dish', 'menuFactory', 'favoriteFactory', 'baseURL', '$ionicPlatform', '$cordovaLocalNotification', '$cordovaToast', function($scope, $stateParams, $ionicPopover, $ionicModal, $timeout, $ionicLoading, dish, menuFactory, favoriteFactory, baseURL, $ionicPlatform, $cordovaLocalNotification, $cordovaToast ) {

    $scope.baseURL = baseURL;
    $scope.showDish = false;
    $scope.message="Loading ...";
    $scope.dish = dish;
    
    //[ BEGIN CODE FOR IMPLEMENTING THE POPOVER ]
    $ionicPopover.fromTemplateUrl('templates/dish-detail-popover.html', {
        scope: $scope
    }).then(function(popover) {
        $scope.popover = popover;
    });

    //Opening the PopOver
    $scope.openPopover = function($event) {
        $scope.popover.show($event);
    };
    
    //Closing the PopOver 
    $scope.closePopover = function() {
        $scope.popover.hide();
    };
    
     $scope.addFavorite = function () {
        console.log("addFavorite on DishDetailController index is " + $stateParams.id);
        //mandatory using parseInt for having an integer from string
        favoriteFactory.addToFavorites(parseInt($stateParams.id));
         
        //Calling for the popover closing
        $scope.closePopover();
    }
    //[ END CODE FOR IMPLEMENTING THE POPOVER ]
     
     $ionicPlatform.ready(function () {
       /** $cordovaLocalNotification.schedule({
                    id: 1,
                    title: "Added Favorite",
                    text: $scope.dish.name
                }).then(function () {
                    console.log('Added Favorite '+$scope.dish.name);
                },
                function () {
                    console.log('Failed to add Notification ');
                });

                $cordovaToast
                  .show('Added Favorite '+$scope.dish.name, 'long', 'bottom')
                  .then(function (success) {
                      // success
                  }, function (error) {
                      // error
                  });*/
            });
     
    //[ BEGIN CODE FOR IMPLEMENTING THE MODAL POPUP FOR COMMENTS ON DISH ]
    $ionicModal.fromTemplateUrl('templates/dish-comment.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.modal = modal;
    });
    
    // Triggered in the comment modal to close it
    $scope.closeComment = function() {
        $scope.modal.hide();
        $scope.closePopover();
    };

    // Open the comment modal called by ng-click="comment()"
    $scope.addComment = function() {
        $scope.modal.show();
    };
    
    //Step 1: Create a JavaScript object to hold the comment from the form
    $scope.comment = {rating:5, comment:"", author:"", date:"" };
    
    $scope.submitComment = function () {
            console.log($scope.comment);
            //Step 2: This is how you record the date
            $scope.comment.date = new Date().toISOString();
            // Step 3: Push your comment into the dish's comment array
            $scope.dish.comments.push($scope.comment);

            //Using update per i commenti a db.json
            menuFactory.update({id:$scope.dish.id}, $scope.dish);

            //Step 4: reset your form to pristine
            //$scope.commentForm.$setPristine();
            console.log($scope.comment);
            //Step 5: reset your JavaScript object that holds your comment
            $scope.comment = {rating:5, comment:"", author:"", date:"" }; 
            $scope.closeComment();
            $scope.closePopover();
    };
    //[ END CODE FOR IMPLEMENTING THE MODAL POPUP FOR COMMENTS ON DISH ]
    
}]).controller('IndexController', ['$scope', 'menuFactory', 'promotionFactory', 'corporateFactory', 'dish',  'promotion', 'leader','baseURL', function($scope, menuFactory, promotionFactory, corporateFactory, dish, promotion, leader, baseURL) {
   
    $scope.baseURL = baseURL;
    $scope.showDish = false;
    $scope.message="Loading ...";
    
    //Variable from resolve injected from app.js
    $scope.dish = dish;
    $scope.promotion = promotion;
    $scope.leader = leader;
    
    
   
    
}]).controller('AboutController', ['$scope', '$stateParams', 'corporateFactory', 'leaders','baseURL', function($scope, $stateParams, corporateFactory, leaders, baseURL) {

    $scope.baseURL = baseURL;
    $scope.showLeaders = false;
    $scope.message="Loading ...";
    
    //Variable injected from resolve on app.js
    $scope.leaders = leaders;
    
}]).controller('FavoritesController', ['$scope', 'dishes', 'favorites', 'favoriteFactory', 'baseURL', '$ionicListDelegate', '$ionicPopup', '$ionicLoading', '$timeout', '$ionicPlatform', '$cordovaVibration', function ($scope, dishes, favorites, favoriteFactory, baseURL, $ionicListDelegate, $ionicPopup, $ionicLoading, $timeout,$ionicPlatform,$cordovaVibration ) {

    $scope.baseURL = baseURL;
    $scope.shouldShowDelete = false;
    
    //Variable injected with resolve from app.js
    $scope.favorites = favorites;
    $scope.dishes = dishes;
    
    console.log($scope.dishes, $scope.favorites);

    $scope.toggleDelete = function () {
        $scope.shouldShowDelete = !$scope.shouldShowDelete;
        console.log($scope.shouldShowDelete);
    }

    $scope.deleteFavorite = function (index) {

        var confirmPopup = $ionicPopup.confirm({
            title: 'Confirm Delete',
            template: 'Are you sure you want to delete this item?'
        });

        confirmPopup.then(function (res) {
            if (res) {
                console.log('Ok to delete');
                favoriteFactory.deleteFromFavorites(index);
            } else {
                console.log('Canceled delete');
            }
        });

        $ionicPlatform.ready(function(){
          $cordovaVibration.vibrate(100);
        });
        
        $scope.shouldShowDelete = false;

    }
    
}]).filter('favoriteFilter', function () {
    return function (dishes, favorites) {
        var out = [];
        for (var i = 0; i < favorites.length; i++) {
            for (var j = 0; j < dishes.length; j++) {
                if (dishes[j].id === favorites[i].id)
                    out.push(dishes[j]);
            }
        }
        return out;

    }});
