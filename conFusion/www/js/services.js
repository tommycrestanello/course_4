'use strict';

angular.module('conFusion.services',['ngResource'])
        .constant("baseURL","http://localhost:3000/")
        //.service('menuFactory', ['$resource', 'baseURL', function($resource,baseURL) {
        .factory('menuFactory', ['$resource', 'baseURL', function($resource,baseURL) {  
            
            //With developing a factory we don't need a function
            //it's possible to return directly the resource with rsolve in app.js
            
            //this.getDishes = function(){
            return $resource(baseURL+"dishes/:id",null,  {'update':{method:'PUT' }});    
            //};
            
        }]).factory('promotionFactory', ['$resource', 'baseURL', function($resource,baseURL){
    
            var promotions = [
                {
                          _id:0,
                          name:'Weekend Grand Buffet', 
                          image: 'images/buffet.png',
                          label:'New',
                          price:'19.99',
                          description:'Featuring mouthwatering combinations with a choice of five different salads, six enticing appetizers, six main entrees and five choicest desserts. Free flowing bubbly and soft drinks. All for just $19.99 per person ',
                }
                
            ];
    
                //it's possible to return directly the resource with resolve in the app.js
                //this.getPromotion = function() {
                    return  $resource(baseURL+"promotions/:id");;
                //}
    
                        
        }])

        .factory('corporateFactory', ['$resource', 'baseURL', function($resource,baseURL) {
            
            //New version of the factory for using resolve on app.js
            return $resource(baseURL+"leadership/:id",null,{});
        
            //Code of backup previous version of the factory
            //var corpfac = {};
	        //corpfac.getLeaders = function() {
	        //return $resource(baseURL+"leadership/:id",null,{});
            //};
	        //return corpfac;
            //Code of backup previous version of the factory
    
        }])

        .factory('feedbackFactory', ['$resource', 'baseURL', function($resource,baseURL) {
            return $resource(baseURL+"feedback/:id");
    
}]).factory('favoriteFactory', ['$resource', '$localStorage', 'baseURL', function ($resource, $localStorage, baseURL) {
    var favFac = {};
    
    //On loading time of the application getObject from localStorage
    //if exist some data stored on favorites the list will be display
    //otherwise we get an empty js array
    var favorites = $localStorage.getObject('favorites', '[]');

    //adding to favorite method
    favFac.addToFavorites = function (index) {
        console.log("Invoking "+index);
        for (var i = 0; i < favorites.length; i++) {
            if (favorites[i].id == index)
                return;
        }
        favorites.push({id: index});
        //Store data on localStorage
        $localStorage.storeObject('favorites',favorites);
    };
    
    //deleting favorites method
    favFac.deleteFromFavorites = function (index) {
        for (var i = 0; i < favorites.length; i++) {
            if (favorites[i].id == index)
                //calling of the splice method on javascript array
                favorites.splice(i,1);
        }
        //After deleting an object from the favorites list
        //we must update the list actually stored.
        
        //update of the localStorage after removing
        $localStorage.storeObject('favorites',favorites);
    };

    //returning all the list of the favourite
    favFac.getFavorites = function () {
        return favorites;
    };
    return favFac;
    
}]).factory('$localStorage', ['$window', function($window){
    
    //factory for using on our application the locastorage of the browser 
    //on html5. On this storage it's possible to use only string data
    
    return {
        store: function(key,value) {
            $window.localStorage[key]=value;
        },
        get: function(key, defaultValue) {
            return $window.localStorage[key] || defaultValue;       
        },
        storeObject: function(key,value) {
            $window.localStorage[key] = JSON.stringify(value);
        },
        getObject: function(key, defaultValue) {
            return JSON.parse($window.localStorage[key] || defaultValue);
        }
    }
    
}]);